function up_we_go()
    local success, data = turtle.inspectUp()
    if success then
        for _, i in pairs(known_leaves) do
            if i == data.name then
                turtle.digUp()
            end
        end
    end

    if turtle.detectUp() == false then
        turtle.up()
    end
end

function down_we_go()
    local success, data = turtle.inspectDown()
    if success then
        for _, i in pairs(known_leaves) do
            if i == data.name then
                turtle.digDown()
            end
        end
    end

    if turtle.detectDown() == false then
        turtle.down()
    else
        go_mode = "check"
    end
end

function cut_wood()
    local success, data = turtle.inspect()
    if success then
        for _, i in pairs(known_wood) do
            if i == data.name then
                turtle.dig()
                turtle.suck()
                go_mode = "up"
            end
        end
    else
        go_mode = "down"
    end
end

function check()
    if turtle.detect() == false then
        for i=1,16 do
            turtle.select(i)
            item_detail = turtle.getItemDetail()
            if item_detail then
                for _, x in pairs(known_saplings) do
                    if x == item_detail.name then
                        turtle.place()
                        break
                    end
                end
            end
        end
    end

    local success, data = turtle.inspect()
    if success then
        for _, i in pairs(known_wood) do
            if i == data.name then
                go_mode = "up"
            end
        end
    end
    sleep(10)
    turtle.suck()
end

function keep_fueled()
    if turtle.getFuelLevel() < 80 then
        for i=1,16 do
            turtle.select(i)
            if turtle.getItemCount() > 0 then
                item_detail = turtle.getItemDetail()
                if item_detail then
                    if contains(known_fuels, item_detail.name) then
                        turtle.refuel()
                        break
                    end
                end
            end
        end
        turtle.select(1)
    end
end

function contains(list, x)
    for _, v in pairs(list) do
        if v == x then
            return true
        end
    end
    return false
end


function select_first_free_slot()
    slot = 1
    while turtle.getItemSpace(slot) == 0 do
        if slot == 16 then
            break
        else
            slot = slot + 1
        end
    end
end

-- STARTING POINT

print("Work, work, work.")
print("I hope you gave me some fuel!")

known_fuels = {
    "minecraft:coal"
}

known_wood = {
    "minecraft:log",
    "ic2:rubber_wood"
}

known_leaves = {
    "minecraft:leaves",
    "ic2:leaves"
}

known_saplings = {
    "ic2:sapling",
    "minecraft:sapling",
    "forestry:sapling",
    "biomesoplenty:sapling_0",
    "biomesoplenty:sapling_1",
    "biomesoplenty:sapling_2",
    "natura:overworld_sapling",
    "natura:overworld_sapling2",
    "natura:redwood_sapling",
    "natura:nether_sapling",
    "quark:variant_sapling",
    "rustic:sapling",
    "rustic:sapling_apple"
}

go_mode = "up"

while true do
    keep_fueled()
    if go_mode == "check" then
        check()
    end
    if go_mode == "up" then
        cut_wood()
        up_we_go()
    elseif go_mode == "down" then
        cut_wood()
        down_we_go()
    end
end
