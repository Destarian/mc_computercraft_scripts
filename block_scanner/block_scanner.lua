cn = 0

-- STARTING POINT
while true do
    local scanner = peripheral.wrap('back')
    local str_to_print = cn .. "\n"

    local function show_pos(n, x, y, z)
        if z < 0 then
            dir_y = "N" .. tostring(math.abs(z))
        elseif z > 0 then
            dir_y = "S" .. tostring(math.abs(z))
        else
            dir_y = ""
        end

        if x < 0 then
            dir_x = "W" .. tostring(math.abs(x))
        elseif x > 0 then
            dir_x = "E" .. tostring(math.abs(x))
        else
            dir_x = ""
        end

        if y < 0 then
            dir_z = "B" .. tostring(math.abs(y))
        elseif y > 0 then
            dir_z = "A" .. tostring(math.abs(y))
        else
            dir_z = ""
        end

        return n .. " " .. dir_y .. " " .. dir_x .. " " .. dir_z .. "\n"
    end

    for _, block in pairs(scanner.scan()) do
        if block.name == "thermalfoundation:ore" then
            local scan_meta = textutils.serialise(scanner.getBlockMeta(block.x, block.y, block.z).state.type)
            if scan_meta == '"IRIDIUM"' then
                str_to_print = str_to_print .. show_pos('IRIDIUM', block.x, block.y, block.z)
            end
        end
    end
    for _, block in pairs(scanner.scan()) do
        if block.name == "minecraft:lapis_ore" then
            str_to_print = str_to_print .. show_pos('LAPIS', block.x, block.y, block.z)
        end
    end
    term.setCursorPos(1,1)
    term.clear()
    print(str_to_print)
    cn = cn + 1
    sleep(0.5)
end
