function keep_fueled()
    if turtle.getFuelLevel() < 80 then
        for i=1,16 do
            turtle.select(i)
            if turtle.getItemCount() > 0 then
                item_detail = turtle.getItemDetail()
                if item_detail then
                    if contains(known_fuels, item_detail.name) then
                        turtle.refuel()
                        break
                    end
                end
            end
        end
        turtle.select(1)
    end
end


function farm()
    local success, data = turtle.inspectDown()
    if success then
        if data.name == "minecraft:wheat" then
            if data.state.age == 7 then
                select_first_free_slot()
                turtle.digDown()
            end
        end
    end
    -- try to plant if there's nothing below
    local success, data = turtle.inspectDown()
    if not success then
        for i=1,16 do
            turtle.select(i)
            if turtle.getItemCount() > 0 then
                if turtle.getItemDetail().name == "minecraft:wheat_seeds" then
                    turtle.placeDown()
                    break
                end
            end
        end
    end
    select_first_free_slot()
end


function movement(row, total_rows)
    if turtle.detect() == true then

        if turn_mode == "left" and row ~= total_rows then
            rotate_left_n_go()
            turn_mode = "right"
        elseif turn_mode == "right" and row ~= total_rows then
            rotate_right_n_go()
            turn_mode = "left"
        elseif turn_mode == "left" and row == total_rows then
            rotate_right_n_go()
            turn_mode = "left"
        elseif turn_mode == "right" and row == total_rows then
            rotate_left_n_go()
            turn_mode = "right"
        end

        row = increment_row(row, total_rows)
        return row

    else
        turtle.forward()
        return row
    end
end


function increment_row(row, total_rows)
    if row == total_rows then
        row = 2
        sleep(60)
    else
        row = row + 1
    end
    return row
end


function rotate_left_n_go()
    turtle.turnLeft()
    turtle.forward()
    turtle.turnLeft()
end


function rotate_right_n_go()
    turtle.turnRight()
    turtle.forward()
    turtle.turnRight()
end


function dump_to_chest()
    local success, data = turtle.inspectDown()
    if success then
        if data.name == "ironchest:iron_chest" then
            for i=1,16 do
                turtle.select(i)
                if turtle.getItemCount() > 0 then
                    if contains(known_fuels, turtle.getItemDetail().name) == false then
                        turtle.dropDown(64)
                    end
                end
            end
        end
    end
end


function contains(list, x)
    for _, v in pairs(list) do
        if v == x then
            return true
        end
    end
    return false
end


function select_first_free_slot()
    slot = 1
    while turtle.getItemSpace(slot) == 0 do
        if slot == 16 then
            break
        else
            slot = slot + 1
        end
    end
end


-- STARTING POINT

print("Hope you've put some kind of storage chest for me. I can't carry it all")
print("Work, work, work.")

known_fuels = {"minecraft:coal"}

turn_mode = "left"
row = 1

-- set total_rows to number of rows on your plant to let the turtle know when to turn
total_rows = 3

while true do
    keep_fueled()
    farm()
    row = movement(row, total_rows)
    dump_to_chest()
end

-- do not dump fuel into the chest
