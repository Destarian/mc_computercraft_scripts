function dig_forward()
    success, data = turtle.inspect()
    if success then
        if data.name == "minecraft:cobblestone" then
            turtle.dig()
            turtle.forward()
        end
    else
        print('To nima cobblestone')
        mode = "go_down"
    end
end

function go_down()
    local success, data = turtle.inspectDown()
    if success then
        if data.name == "minecraft:cobblestone" then
            turtle.digDown()
            turtle.down()
            for i=1,4 do
                turtle.turnLeft()
                local success, data = turtle.inspect()
                if success then
                    if data.name == "minecraft:cobblestone" then
                        mode = "forward"
                        break
                    end
                else
                    mode = "go_down"
                end
            end
        end
    end
end

-- STARTING POINT

print("Work, work, work.")

mode = "forward"

while true do
    if mode == "forward" then
        dig_forward()
    elseif mode == "go_down" then
        go_down()
    end
end
