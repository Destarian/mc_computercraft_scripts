-- STARTING POINT

function turn_off()
    print("SIGNAL OFF")
    redstone.setOutput('left', false)
end

function turn_on()
    print("SIGNAL ON")
    redstone.setOutput('left', true)
end

while true do
    local s = peripheral.wrap('right').getMetaByID("4e163cbc-88b1-492e-ac7a-861f141bb36e")

    if s == nil then
        turn_off()
        print('DESTARIAN: NOPE')
    elseif s.name == "Destarian" then
        turn_on()
        print('DESTARIAN: ON')
    end
end
