function keep_fueled()
    if turtle.getFuelLevel() < 80 then
        for i=1,16 do
            turtle.select(i)
            if turtle.getItemCount() > 0 then
                item_detail = turtle.getItemDetail()
                if item_detail then
                    if contains(known_fuels, item_detail.name) then
                        turtle.refuel()
                        break
                    end
                end
            end
        end
        turtle.select(1)
    end
end


function farm_sugar_cane_and_move_arse()
    local success, data = turtle.inspect()
    if success then
        if data.name == "minecraft:reeds" then
            if turtle.getItemSpace() == 0 then
                dump_to_chest()
            end
            turtle.dig()
        else
            if data.name == "ironchest:iron_chest" then
                dump_to_chest()
                sleep(90)
            end
            turtle.turnRight()
        end
    else
        turtle.suck()
        turtle.suckDown()
        turtle.forward()
    end
end


function dump_to_chest()
    for i=1,16 do
        turtle.select(i)
        if turtle.getItemCount() > 0 then
            if contains(known_fuels, turtle.getItemDetail().name) == false then
                turtle.drop(64)
            end
        end
    end
end


function contains(list, x)
    for _, v in pairs(list) do
        if v == x then
            return true
        end
    end
    return false
end


function select_first_free_slot()
    slot = 1
    while turtle.getItemSpace(slot) == 0 do
        if slot == 16 then
            break
        else
            slot = slot + 1
        end
    end
end


-- STARTING POINT

print("Hope you've put some kind of storage chest for when I turn right. I can't carry it all")
print("Also don't forget to give me some fuel!")
print("Work, work, work.")

known_fuels = {"minecraft:coal"}

while true do
    keep_fueled()
    select_first_free_slot()
    farm_sugar_cane_and_move_arse()
end
